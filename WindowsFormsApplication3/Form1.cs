﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class BarcodeCountry : Form
    {
        private Countries CurrentCountry;
        public BarcodeCountry()
        {
            InitializeComponent();
            NewScan.Hide();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char charCheck = e.KeyChar;
            if (!Char.IsDigit(charCheck) && charCheck != 8 && charCheck != (char)Keys.Back)
            {
                e.Handled = true;
            }
            if (charCheck == (char)Keys.Enter && textBox1.TextLength > 0)
            {
                Scan.Hide();
                textBox1.Enabled = false;
                ChangeMade();
                NewScan.Show();
            }

            if (textBox1.TextLength > 2)
            {
                Scan.Focus();
                e.Handled = true;
            }

            if (textBox1.Text == string.Empty && (e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
            }



        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = string.Empty;
            picture.Visible = false;
        }

        private void ChangeMade()
        {
            int number = int.Parse(textBox1.Text);
            bool codeFound = true;

            ////////////////////////////////////////////////////////////////////
            ////                 Beginning of IF statements                 ////
            ////////////////////////////////////////////////////////////////////}

            if (number == 999)
            {
                CurrentCountry = Countries.Nuleva;
            }
            if (number < 140)
            {
                CurrentCountry = Countries.UnitedStates;
            }

            if (number > 300 && number < 379)
            {
                CurrentCountry = Countries.FranceandMonaco;
            }

            if (number == 380)
            {
                CurrentCountry = Countries.Bulgaria;
            }

            if (number == 383)
            {
                CurrentCountry = Countries.Slovenia;
            }

            if (number == 385)
            {
                CurrentCountry = Countries.Croatia;
            }

            if (number == 387)
            {
                CurrentCountry = Countries.Bosnia;
            }

            if (number == 389)
            {
                CurrentCountry = Countries.Montenegro;
            }

            if (number == 390)
            {
                CurrentCountry = Countries.Kosovo;
            }

            if (number > 400 && number < 441 )
            {
                CurrentCountry = Countries.Germany;
            }

            if (number > 440 && number < 460 )
            {
                CurrentCountry = Countries.Japan;
            }

            if (number > 459 && number < 470 )
            {
                CurrentCountry = Countries.Russia;
            }

            if (number == 470)
            {
                CurrentCountry = Countries.Kyrgistan;
            }

            if (number == 471)
            {
                CurrentCountry = Countries.Taiwan;
            }

            if (number == 474)
            {
                CurrentCountry = Countries.Estonia;
            }

            if (number == 475)
            {
                CurrentCountry = Countries.Latvia;
            }

            if (number == 476)
            {
                CurrentCountry = Countries.Azerbaijan;
            }

            if (number == 477)
            {
                CurrentCountry = Countries.Lithuania;
            }

            if (number == 478)
            {
                CurrentCountry = Countries.Uzbekistan;
            }

            if (number == 479)
            {
                CurrentCountry = Countries.SriLanka;
            }

            if (number == 480)
            {
                CurrentCountry = Countries.Philipines;
            }

            if (number == 481)
            {
                CurrentCountry = Countries.Belarus;
            }

            if (number == 482)
            {
                CurrentCountry = Countries.Ukraine;
            }

            if (number == 483)
            {
                CurrentCountry = Countries.Turkmenistan;
            }

            if (number == 484)
            {
                CurrentCountry = Countries.Moldova;
            }

            if (number == 485)
            {
                CurrentCountry = Countries.Armenia;
            }

            if (number == 486)
            {
                CurrentCountry = Countries.Georgia;
            }

            if (number == 487)
            {
                CurrentCountry = Countries.Kazahstan;
            }

            if (number == 488)
            {
                CurrentCountry = Countries.Tajikistan;
            }

            if (number == 489)
            {
                CurrentCountry = Countries.HongKong;
            }

            if (number > 489 && number < 500)
            {
                CurrentCountry = Countries.Japan;
            }

            if (number > 500 && number < 510 )
            {
                CurrentCountry = Countries.UK;
            }

            if (number > 519 && number < 522)
            {
                CurrentCountry = Countries.Greece;
            }

            if (number == 528)
            {
                CurrentCountry = Countries.Lebanon;
            }

            if (number == 529)
            {
                CurrentCountry = Countries.Cyprus;
            }

            if (number == 530)
            {
                CurrentCountry = Countries.Albania;
            }

            if (number == 531)
            {
                CurrentCountry = Countries.Macedonia;
            }

            if (number == 535)
            {
                CurrentCountry = Countries.Malta;
            }

            if (number == 539)
            {
                CurrentCountry = Countries.Ireland;
            }

            if (number > 539 && number < 550)
            {
                CurrentCountry = Countries.Belgium;
            }

            if (number == 560)
            {
                CurrentCountry = Countries.Portugal;
            }

            if (number == 569)
            {
                CurrentCountry = Countries.Iceland;
            }

            if (number > 569 && number < 580)
            {
                CurrentCountry = Countries.Denmark;
            }

            if (number == 590)
            {
                CurrentCountry = Countries.Poland;
            }

            if (number == 594)
            {
                CurrentCountry = Countries.Romania;
            }

            if (number == 599)
            {
                CurrentCountry = Countries.Hungary;
            }

            if (number > 599 && number < 602)
            {
                CurrentCountry = Countries.SouthAfrica;
            }

            if (number == 603)
            {
                CurrentCountry = Countries.Ghana;
            }

            if (number == 604)
            {
                CurrentCountry = Countries.Senegal;
            }


            if (number == 608)
            {
                CurrentCountry = Countries.Bahrain;
            }

            if (number == 609)
            {
                CurrentCountry = Countries.Mauritius;
            }

            if (number == 611)
            {
                CurrentCountry = Countries.Morocco;
            }

            if (number == 613)
            {
                CurrentCountry = Countries.Algeria;
            }

            if (number == 615)
            {
                CurrentCountry = Countries.Nigeria;
            }

            if (number == 616)
            {
                CurrentCountry = Countries.Kenya;
            }

            if (number == 618)
            {
                CurrentCountry = Countries.IvoryCoast;
            }

            if (number == 619)
            {
                CurrentCountry = Countries.Tunisia;
            }

            if (number == 620)
            {
                CurrentCountry = Countries.Tanzania;
            }

            if (number == 621)
            {
                CurrentCountry = Countries.Syria;
            }

            if (number == 622)
            {
                CurrentCountry = Countries.Egypt;
            }

            if (number == 623)
            {
                CurrentCountry = Countries.Brunei;
            }

            if (number == 624)
            {
                CurrentCountry = Countries.Libya;
            }

            if (number == 625)
            {
                CurrentCountry = Countries.Jordan;
            }

            if (number == 626)
            {
                CurrentCountry = Countries.Iran;
            }

            if (number == 627)
            {
                CurrentCountry = Countries.Kuwait;
            }

            if (number == 628)
            {
                CurrentCountry = Countries.SaudiArabia;
            }

            if (number == 629)
            {
                CurrentCountry = Countries.UAR;
            }

            if (number > 639 && number < 650)
            {
                CurrentCountry = Countries.Finland;
            }

            if (number > 689 && number < 700)
            {
                CurrentCountry = Countries.China;
            }

            if (number > 700 && number < 710 )
            {
                CurrentCountry = Countries.Norway;
            }

            if (number == 729)
            {
                CurrentCountry = Countries.Israel;
            }

            if (number > 729 && number < 740 )
            {
                CurrentCountry = Countries.Sweden;
            }

            if (number == 740)
            {
                CurrentCountry = Countries.Guatemala;
            }

            if (number == 741)
            {
                CurrentCountry = Countries.ElSalvador;
            }

            if (number == 742)
            {
                CurrentCountry = Countries.Honduras;
            }

            if (number == 743)
            {
                CurrentCountry = Countries.Nicaragua;
            }

            if (number == 744)
            {
                CurrentCountry = Countries.CostaRica;
            }

            if (number == 745)
            {
                CurrentCountry = Countries.Panama;
            }

            if (number == 746)
            {
                CurrentCountry = Countries.DominicanRepublic;
            }

            if (number == 750)
            {
                CurrentCountry = Countries.Mexico;
            }

            if (number == 755 || number == 756 )
            {
                CurrentCountry = Countries.Canada;
            }

            if (number == 759)
            {
                CurrentCountry = Countries.Venezuela;
            }

            if (number > 759 && number < 770)
            {
                CurrentCountry = Countries.Switzerland_Lichtenstein;
            }

            if (number == 770 || number == 771  )
            {
                CurrentCountry = Countries.Colombia;
            }

            if (number == 773)
            {
                CurrentCountry = Countries.Urugay;
            }

            if (number == 775)
            {
                CurrentCountry = Countries.Peru;
            }

            if (number == 777)
            {
                CurrentCountry = Countries.Bolivia;
            }

            if (number == 778 || number == 779)
            {
                CurrentCountry = Countries.Argentina;
            }

            if (number == 780)
            {
                CurrentCountry = Countries.Chilie;
            }

            if (number == 784)
            {
                CurrentCountry = Countries.Paraguay;
            }

            if (number == 786)
            {
                CurrentCountry = Countries.Ecuador;
            }

            if (number > 789 && number < 791 )
            {
                CurrentCountry = Countries.Brazil;
            }

            if (number > 799 && number < 840)
            {
                CurrentCountry = Countries.Italy_SanMarino_VaticanCity;
            }

            if (number > 840 && number < 850)
            {
                CurrentCountry = Countries.Spain_Andorra;
            }

            if (number == 850)
            {
                CurrentCountry = Countries.Cuba;
            }

            if (number == 858)
            {
                CurrentCountry = Countries.Slovacia;
            }

            if (number == 859)
            {
                CurrentCountry = Countries.Czech;
            }

            if (number == 860)
            {
                CurrentCountry = Countries.Serbia;
            }

            if (number == 865)
            {
                CurrentCountry = Countries.Mongolia;
            }

            if (number == 867)
            {
                CurrentCountry = Countries.North_Korea;
            }

            if (number > 867 && number < 870)
            {
                CurrentCountry = Countries.Turkey;
            }

            if (number > 869 && number <880)
            {
                CurrentCountry = Countries.Netherlands;
            }

            if (number == 880)
            {
                CurrentCountry = Countries.South_Korea;
            }

            if (number == 884)
            {
                CurrentCountry = Countries.Cambodia;
            }

            if (number == 885)
            {
                CurrentCountry = Countries.Thailand;
            }

            if (number == 888)
            {
                CurrentCountry = Countries.Singapoure;
            }

            if (number == 890)
            {
                CurrentCountry = Countries.India;
            }

            if (number == 893)
            {
                CurrentCountry = Countries.Vietnam;
            }

            if (number == 896)
            {
                CurrentCountry = Countries.Pakistan;
            }

            if (number == 899)
            {
                CurrentCountry = Countries.Indonesia;
            }

            if (number > 899 && number < 920 )
            {
                CurrentCountry = Countries.Austria;
            }

            if (number > 929 && number < 940 )
            {
                CurrentCountry = Countries.Australia;
            }

            if (number > 939 && number < 950)
            {
                CurrentCountry = Countries.New_Zealand;
            }

            if (number == 955)
            {
                CurrentCountry = Countries.Malaysia;
            }

            if (number == 958)
            {
                CurrentCountry = Countries.Macau;
            }


            ////////////////////////////////////////////////////////////////////
            ////                    END of IF statements                    ////
            ////                    Beginning of switch                     ////
            ////////////////////////////////////////////////////////////////////

            if (CurrentCountry == 0 || CurrentCountry == Countries.NoCountry)
            {
                MessageBox.Show("Няма такъв код в базата данни", "Грешка");
                CurrentCountry = Countries.NoCountry;
                textBox1.Focus();
            }
            switch (CurrentCountry)
            {
                case (Countries.Bulgaria):
                    {
                        label3.Text = "България";
                        textBox1.Text = "България";
                        picture.Image = Properties.Resources.Flag_of_Bulgaria_svg;
                        break;
                    }

                case (Countries.UnitedStates):
                    {
                        label3.Text = "Америка";
                        textBox1.Text = "Америка";
                        picture.Image = Properties.Resources.America;
                        break;
                    }

                case (Countries.FranceandMonaco):
                    {
                        label3.Text = "Франция";
                        textBox1.Text = "Франция";
                        picture.Image = Properties.Resources.fr;
                        break;
                    }
                case (Countries.Slovenia):
                    {
                        label3.Text = "Словения";
                        textBox1.Text = "Словения";
                        picture.Image = Properties.Resources.si;
                        break;
                    }
                case (Countries.Croatia):
                    {
                        label3.Text = "Хърватия";
                        textBox1.Text = "Хърватия";
                        picture.Image = Properties.Resources.hr;
                        break;
                    }
                case (Countries.Bosnia):
                    {
                        label3.Text = "Босня и херцеговина";
                        textBox1.Text = "Босня и херцеговина";
                        picture.Image = Properties.Resources.ba;
                        break;
                    }

                case (Countries.Montenegro): //389
                    {
                        label3.Text = "Монтенегро";
                        textBox1.Text = "Монтенегро";
                        picture.Image = Properties.Resources.me;
                        break;
                    }

                case (Countries.Kosovo): //390
                    {
                        label3.Text = "Косово";
                        textBox1.Text = "Косово";
                        picture.Image = Properties.Resources.ks;
                        break;
                    }

                case (Countries.Germany): //400-440
                    {
                        label3.Text = "Германия";
                        textBox1.Text = "Германия";
                        picture.Image = Properties.Resources.de;
                        break;
                    }

                case (Countries.Japan): //450-459 ; //490-499
                    {
                        label3.Text = "Япония";
                        textBox1.Text = "Япония";
                        picture.Image = Properties.Resources.jp;
                        break;
                    }

                case (Countries.Russia)://460-469
                    {
                        label3.Text = "Русия";
                        textBox1.Text = "Русия";
                        picture.Image = Properties.Resources.ru;
                        break;
                    }

                case (Countries.Kyrgistan): //470
                    {
                        label3.Text = "Киргистан";
                        textBox1.Text = "Киргистан";
                        picture.Image = Properties.Resources.kg;
                        break;
                    }

                case (Countries.Taiwan): //471
                    {
                        label3.Text = "Тайван";
                        textBox1.Text = "Тайван";
                        picture.Image = Properties.Resources.tw;
                        break;
                    }

                case (Countries.Estonia): //474
                    {
                        label3.Text = "Естония";
                        textBox1.Text = "Естоиня";
                        picture.Image = Properties.Resources.ee;
                        break;
                    }

                case (Countries.Latvia): //475
                    {
                        label3.Text = "Латвия";
                        textBox1.Text = "Латвия";
                        picture.Image = Properties.Resources.lv;
                        break;
                    }

                case (Countries.Azerbaijan): //476
                    {
                        label3.Text = "Азербайджан";
                        textBox1.Text = "Азербайджан";
                        picture.Image = Properties.Resources.az;
                        break;
                    }

                case (Countries.Lithuania): //477
                    {
                        label3.Text = "Литва";
                        textBox1.Text = "Литва";
                        picture.Image = Properties.Resources.lt;
                        break;
                    }

                case (Countries.Uzbekistan): //478
                    {
                        label3.Text = "Узбекистан";
                        textBox1.Text = "Узбекистан";
                        picture.Image = Properties.Resources.uz;
                        break;
                    }

                case (Countries.SriLanka): //479
                    {
                        label3.Text = "Шри Ланка";
                        textBox1.Text = "Шри Ланка";
                        picture.Image = Properties.Resources.lk;
                        break;
                    }

                case (Countries.Philipines): //480
                    {
                        label3.Text = "Филипини";
                        textBox1.Text = "Филипини";
                        picture.Image = Properties.Resources.ph;
                        break;
                    }

                case (Countries.Belarus): //481
                    {
                        label3.Text = "Беларус";
                        textBox1.Text = "Беларус";
                        picture.Image = Properties.Resources.by;
                        break;
                    }

                case (Countries.Ukraine): //482
                    {
                        label3.Text = "Украйна";
                        textBox1.Text = "Украйна";
                        picture.Image = Properties.Resources.ua;
                        break;
                    }

                case (Countries.Turkmenistan): //483
                    {
                        label3.Text = "Туркменистан";
                        textBox1.Text = "Туркменистан";
                        picture.Image = Properties.Resources.tm;
                        break;
                    }

                case (Countries.Moldova): //484
                    {
                        label3.Text = "Молдова";
                        textBox1.Text = "Молдова";
                        picture.Image = Properties.Resources.md;
                        break;
                    }

                case (Countries.Armenia): //485
                    {
                        label3.Text = "Армения";
                        textBox1.Text = "Армения";
                        picture.Image = Properties.Resources.am;
                        break;
                    }

                case (Countries.Georgia): //486
                    {
                        label3.Text = "Джорджия";
                        textBox1.Text = "Джорджия";
                        picture.Image = Properties.Resources.ge;
                        break;
                    }

                case (Countries.Kazahstan): //487
                    {
                        label3.Text = "Казахстан";
                        textBox1.Text = "Казахстан";
                        picture.Image = Properties.Resources.kz;
                        break;
                    }

                case (Countries.Tajikistan): //488
                    {
                        label3.Text = "Таджикистан";
                        textBox1.Text = "Таджикистан";
                        picture.Image = Properties.Resources.tj;
                        break;
                    }

                case (Countries.HongKong): //489
                    {
                        label3.Text = "Хонг Конг";
                        textBox1.Text = "Хонг Конг";
                        picture.Image = Properties.Resources.hk;
                        break;
                    }

                case (Countries.UK): //500-509
                    {
                        label3.Text = "Англия";
                        textBox1.Text = "Англия";
                        picture.Image = Properties.Resources.gb;
                        break;
                    }

                case (Countries.Greece): //520-521
                    {
                        label3.Text = "Гърция";
                        textBox1.Text = "Гърция";
                        picture.Image = Properties.Resources.gr;
                        break;
                    }

                case (Countries.Lebanon): //528
                    {
                        label3.Text = "Ливан";
                        textBox1.Text = "Ливан";
                        picture.Image = Properties.Resources.lb;
                        break;
                    }

                case (Countries.Cyprus): //529
                    {
                        label3.Text = "Кипър";
                        textBox1.Text = "Кипър";
                        picture.Image = Properties.Resources.cy;
                        break;
                    }

                case (Countries.Albania): //530
                    {
                        label3.Text = "Албания";
                        textBox1.Text = "Албания";
                        picture.Image = Properties.Resources.al;
                        break;
                    }

                case (Countries.Macedonia): //531
                    {
                        label3.Text = "Македония";
                        textBox1.Text = "Македония";
                        picture.Image = Properties.Resources.mk;
                        break;
                    }

                case (Countries.Malta): //535
                    {
                        label3.Text = "Малта";
                        textBox1.Text = "Малта";
                        picture.Image = Properties.Resources.mt;
                        break;
                    }

                case (Countries.Ireland): //539
                    {
                        label3.Text = "Ирландия";
                        textBox1.Text = "Ирландия";
                        picture.Image = Properties.Resources.ie;
                        break;
                    }

                case (Countries.Belgium): //540-549
                    {
                        label3.Text = "Белгия и Люксембург";
                        textBox1.Text = "Белгия и Люксембург";
                        picture.Image = Properties.Resources.be;
                        break;
                    }

                case (Countries.Portugal): //560
                    {
                        label3.Text = "Португалия";
                        textBox1.Text = "Португалия";
                        picture.Image = Properties.Resources.pt;
                        break;
                    }

                case (Countries.Iceland): //569
                    {
                        label3.Text = "Исладния";
                        textBox1.Text = "Исладния";
                        picture.Image = Properties.Resources.isl;
                        break;
                    }

                case (Countries.Denmark): //570 -579 
                    {
                        label3.Text = "Дания, острови Фарое, Гренландия";
                        textBox1.Text = "Дания, острови Фарое, Гренландия";
                        picture.Image = Properties.Resources.dk;
                        break;
                    }

                case (Countries.Poland): //590
                    {
                        label3.Text = "Полша";
                        textBox1.Text = "Полша";
                        picture.Image = Properties.Resources.pl;
                        break;
                    }

                case (Countries.Romania): //594
                    {
                        label3.Text = "Румъния";
                        textBox1.Text = "Румъния";
                        picture.Image = Properties.Resources.ro;
                        break;
                    }
                case (Countries.Hungary): //599
                    {
                        label3.Text = "Унгария";
                        textBox1.Text = "Унгария";
                        picture.Image = Properties.Resources.hu;
                        break;
                    }
                case (Countries.SouthAfrica): //600-601
                    {
                        label3.Text = "Южна Африка";
                        textBox1.Text = "Южна Африка";
                        picture.Image = Properties.Resources.saf;
                        break;
                    }
                case (Countries.Ghana): //603
                    {
                        label3.Text = "Гана";
                        textBox1.Text = "Гана";
                        picture.Image = Properties.Resources.gh;
                        break;
                    }
                case (Countries.Senegal): //604
                    {
                        label3.Text = "Сенегал";
                        textBox1.Text = "Сенегал";
                        picture.Image = Properties.Resources.sn;
                        break;
                    }


                case (Countries.Bahrain): //608
                    {
                        label3.Text = "Бахрейн";
                        textBox1.Text = "Бахрейн";
                        picture.Image = Properties.Resources.bh;
                        break;
                    }

                case (Countries.Mauritius): //609
                    {
                        label3.Text = "Маврикий";
                        textBox1.Text = "Маврикий";
                        picture.Image = Properties.Resources.mu;
                        break;
                    }

                case (Countries.Morocco): //611
                    {
                        label3.Text = "Мароко";
                        textBox1.Text = "Мароко";
                        picture.Image = Properties.Resources.ma;
                        break;
                    }

                case (Countries.Algeria): //613
                    {
                        label3.Text = "Алжир";
                        textBox1.Text = "Алжир";
                        picture.Image = Properties.Resources.alg;
                        break;
                    }

                case (Countries.Nigeria): //615
                    {
                        label3.Text = "Нигерия";
                        textBox1.Text = "Нигерия";
                        picture.Image = Properties.Resources.ng;
                        break;
                    }

                case (Countries.Kenya): //616
                    {
                        label3.Text = "Кения";
                        textBox1.Text = "Кения";
                        picture.Image = Properties.Resources.ke;
                        break;
                    }

                case (Countries.IvoryCoast): //618
                    {
                        label3.Text = "Кот д'Ивоар";
                        textBox1.Text = "Кот д'Ивоар";
                        picture.Image = Properties.Resources.ico;
                        break;
                    }

                case (Countries.Tunisia): //619
                    {
                        label3.Text = "Тунис";
                        textBox1.Text = "Тунис";
                        picture.Image = Properties.Resources.tn;
                        break;
                    }

                case (Countries.Tanzania): //620
                    {
                        label3.Text = "Танзания";
                        textBox1.Text = "Танзания";
                        picture.Image = Properties.Resources.tz;
                        break;
                    }

                case (Countries.Syria): //621
                    {
                        label3.Text = "Сирия";
                        textBox1.Text = "Сирия";
                        picture.Image = Properties.Resources.sy;
                        break;
                    }

                case (Countries.Egypt): //622
                    {
                        label3.Text = "Египет";
                        textBox1.Text = "Египет";
                        picture.Image = Properties.Resources.eg;
                        break;
                    }

                case (Countries.Brunei): //623
                    {
                        label3.Text = "Бруней";
                        textBox1.Text = "Бруней";
                        picture.Image = Properties.Resources.bn;
                        break;
                    }

                case (Countries.Libya): //624
                    {
                        label3.Text = "Либия";
                        textBox1.Text = "Либия";
                        picture.Image = Properties.Resources.ly;
                        break;
                    }

                case (Countries.Jordan): //625
                    {
                        label3.Text = "Йордания";
                        textBox1.Text = "Йордания";
                        picture.Image = Properties.Resources.jo;
                        break;
                    }

                case (Countries.Iran): //626
                    {
                        label3.Text = "Иран";
                        textBox1.Text = "Иран";
                        picture.Image = Properties.Resources._in;
                        break;
                    }

                case (Countries.Kuwait): //627
                    {
                        label3.Text = "Кувейт";
                        textBox1.Text = "Кувейт";
                        picture.Image = Properties.Resources.kw;
                        break;
                    }

                case (Countries.SaudiArabia): //628
                    {
                        label3.Text = "Саудитска Арабия";
                        textBox1.Text = "Саудитска Арабия";
                        picture.Image = Properties.Resources.sa;
                        break;
                    }

                case (Countries.UAR): //629
                    {
                        label3.Text = "ЮАР";
                        textBox1.Text = "ЮАР";
                        picture.Image = Properties.Resources.ae;
                        break;
                    }

                case (Countries.Finland): //640-649
                    {
                        label3.Text = "Финландия";
                        textBox1.Text = "Финландия";
                        picture.Image = Properties.Resources.fi;
                        break;
                    }

                case (Countries.China): //690-699
                    {
                        label3.Text = "Китай";
                        textBox1.Text = "Китай";
                        picture.Image = Properties.Resources.cn;
                        break;
                    }

                case (Countries.Norway): //700-709
                    {
                        label3.Text = "Норвегия";
                        textBox1.Text = "Норвегия";
                        picture.Image = Properties.Resources.no;
                        break;
                    }

                case (Countries.Israel): //729
                    {
                        label3.Text = "Израел";
                        textBox1.Text = "Израел";
                        picture.Image = Properties.Resources.il;
                        break;
                    }

                case (Countries.Sweden): //730-739
                    {
                        label3.Text = "Швеция";
                        textBox1.Text = "Швеция";
                        picture.Image = Properties.Resources.se;
                        break;
                    }

                case (Countries.Guatemala): //740
                    {
                        label3.Text = "Гватемала";
                        textBox1.Text = "Гватемала";
                        picture.Image = Properties.Resources.gt;
                        break;
                    }

                case (Countries.ElSalvador): //741
                    {
                        label3.Text = "Салвадор";
                        textBox1.Text = "Салвадор";
                        picture.Image = Properties.Resources.sv;
                        break;
                    }

                case (Countries.Honduras): //742
                    {
                        label3.Text = "Хондурас";
                        textBox1.Text = "Хондурас";
                        picture.Image = Properties.Resources.hn;
                        break;
                    }

                case (Countries.Nicaragua): //743
                    {
                        label3.Text = "Никарагуа";
                        textBox1.Text = "Никарагуа";
                        picture.Image = Properties.Resources.ni;
                        break;
                    }

                case (Countries.CostaRica): //744
                    {
                        label3.Text = "Коста Рика";
                        textBox1.Text = "Коста Рика";
                        picture.Image = Properties.Resources.cr;
                        break;
                    }

                case (Countries.Panama): //745
                    {
                        label3.Text = "Панама";
                        textBox1.Text = "Панама";
                        picture.Image = Properties.Resources.pa;
                        break;
                    }

                case (Countries.DominicanRepublic): //746
                    {
                        label3.Text = "Доминиканска Република";
                        textBox1.Text = "Доминиканска Република";
                        picture.Image = Properties.Resources._do;
                        break;
                    }

                case (Countries.Mexico): //750
                    {
                        label3.Text = "Мексико";
                        textBox1.Text = "Мексико";
                        picture.Image = Properties.Resources.mx;
                        break;
                    }

                case (Countries.Canada): //754-755
                    {
                        label3.Text = "Канада";
                        textBox1.Text = "Канада";
                        picture.Image = Properties.Resources.ca;
                        break;
                    }

                case (Countries.Venezuela): //759
                    {
                        label3.Text = "Венецуела";
                        textBox1.Text = "Венецуела";
                        picture.Image = Properties.Resources.ve;
                        break;
                    }

                case (Countries.Switzerland_Lichtenstein): //760-769
                    {
                        label3.Text = "Швейцария и Лихтенщайн";
                        textBox1.Text = "Швейцария и Лихтенщайн";
                        picture.Image = Properties.Resources.ch;
                        break;
                    }

                case (Countries.Colombia): //770-771
                    {
                        label3.Text = "Колумбия";
                        textBox1.Text = "Колумбия";
                        picture.Image = Properties.Resources.co;
                        break;
                    }

                case (Countries.Urugay): //773
                    {
                        label3.Text = "Уругвай";
                        textBox1.Text = "Уругвай";
                        picture.Image = Properties.Resources.uy;
                        break;
                    }

                case (Countries.Peru): //775
                    {
                        label3.Text = "Перу";
                        textBox1.Text = "Перу";
                        picture.Image = Properties.Resources.pe;
                        break;
                    }

                case (Countries.Bolivia): //777
                    {
                        label3.Text = "Боливия";
                        textBox1.Text = "Боливия";
                        picture.Image = Properties.Resources.bo;
                        break;
                    }

                case (Countries.Argentina): //778-779
                    {
                        label3.Text = "Аржентина";
                        textBox1.Text = "Аржентина";
                        picture.Image = Properties.Resources.ar;
                        break;
                    }

                case (Countries.Chilie): //780
                    {
                        label3.Text = "Чили";
                        textBox1.Text = "Чили";
                        picture.Image = Properties.Resources.cl;
                        break;
                    }

                case (Countries.Paraguay): //784
                    {
                        label3.Text = "Парагвай";
                        textBox1.Text = "Парагвай";
                        picture.Image = Properties.Resources.py;
                        break;
                    }

                case (Countries.Ecuador): //786
                    {
                        label3.Text = "Еквадор";
                        textBox1.Text = "Еквадор";
                        picture.Image = Properties.Resources.ec;
                        break;
                    }

                case (Countries.Brazil): //789-790
                    {
                        label3.Text = "Бразилия";
                        textBox1.Text = "Бразилия";
                        picture.Image = Properties.Resources.br;
                        break;
                    }

                case (Countries.Italy_SanMarino_VaticanCity): //800-839
                    {
                        label3.Text = "Италия, Сан Марино, Ватикана";
                        textBox1.Text = "Италия, Сан Марино, Ватикана";
                        picture.Image = Properties.Resources.it;
                        break;
                    }

                case (Countries.Spain_Andorra): //840-849
                    {
                        label3.Text = "Испания и Андора";
                        textBox1.Text = "Испания и Андора";
                        picture.Image = Properties.Resources.es;
                        break;
                    }

                case (Countries.Cuba): //850
                    {
                        label3.Text = "Куба";
                        textBox1.Text = "Куба";
                        picture.Image = Properties.Resources.cu;
                        break;
                    }

                case (Countries.Slovacia): //858
                    {
                        label3.Text = "Словакия";
                        textBox1.Text = "Словакия";
                        picture.Image = Properties.Resources.sk;
                        break;
                    }

                case (Countries.Czech): //859
                    {
                        label3.Text = "Чехия";
                        textBox1.Text = "Чехия";
                        picture.Image = Properties.Resources.cz;
                        break;
                    }

                case (Countries.Serbia): //860
                    {
                        label3.Text = "Сърбия";
                        textBox1.Text = "Сърбия";
                        picture.Image = Properties.Resources.rs;
                        break;
                    }

                case (Countries.Mongolia): //865
                    {
                        label3.Text = "Монголя";
                        textBox1.Text = "Монголя";
                        picture.Image = Properties.Resources.mn;
                        break;
                    }

                case (Countries.North_Korea): //867
                    {
                        label3.Text = "Северна Корея";
                        textBox1.Text = "Северна Корея";
                        picture.Image = Properties.Resources.kp;
                        break;
                    }

                case (Countries.Turkey): //868-869
                    {
                        label3.Text = "Турция";
                        textBox1.Text = "Турция";
                        picture.Image = Properties.Resources.tr;
                        break;
                    }

                case (Countries.Netherlands): //870-879
                    {
                        label3.Text = "Холандия";
                        textBox1.Text = "Холандия";
                        picture.Image = Properties.Resources.nl;
                        break;
                    }

                case (Countries.South_Korea): //880
                    {
                        label3.Text = "Южна Кореа";
                        textBox1.Text = "Южна Кореа";
                        picture.Image = Properties.Resources.kr;
                        break;
                    }

                case (Countries.Cambodia): //884
                    {
                        label3.Text = "Камбоджа";
                        textBox1.Text = "Камбоджа";
                        picture.Image = Properties.Resources.kh;
                        break;
                    }

                case (Countries.Thailand): //885
                    {
                        label3.Text = "Тайланд";
                        textBox1.Text = "Тайланд";
                        picture.Image = Properties.Resources.th;
                        break;
                    }

                case (Countries.Singapoure): //888
                    {
                        label3.Text = "Сингапур";
                        textBox1.Text = "Сингапур";
                        picture.Image = Properties.Resources.sg;
                        break;
                    }

                case (Countries.India): //890
                    {
                        label3.Text = "Индия";
                        textBox1.Text = "Индия";
                        picture.Image = Properties.Resources._in;
                        break;
                    }

                case (Countries.Vietnam): //893
                    {
                        label3.Text = "Виетнам";
                        textBox1.Text = "Виетнам";
                        picture.Image = Properties.Resources.vn;
                        break;
                    }

                case (Countries.Pakistan): //896
                    {
                        label3.Text = "Пакистан";
                        textBox1.Text = "Пакистан";
                        picture.Image = Properties.Resources.pk;
                        break;
                    }

                case (Countries.Indonesia): //899
                    {
                        label3.Text = "Индонезия";
                        textBox1.Text = "Индонезия";
                        picture.Image = Properties.Resources.id;
                        break;
                    }

                case (Countries.Austria): //900-919
                    {
                        label3.Text = "Австрия";
                        textBox1.Text = "Австрия";
                        picture.Image = Properties.Resources.at;
                        break;
                    }

                case (Countries.Australia): //930-939
                    {
                        label3.Text = "Австрия";
                        textBox1.Text = "Австрия";
                        picture.Image = Properties.Resources.au;
                        break;
                    }


                case (Countries.New_Zealand): //940-949
                    {
                        label3.Text = "Нова Зеландия";
                        textBox1.Text = "Нова Зеландия";
                        picture.Image = Properties.Resources.nz;
                        break;
                    }

                case (Countries.Malaysia): //955
                    {
                        label3.Text = "Малайзия";
                        textBox1.Text = "Малайзия";
                        picture.Image = Properties.Resources.my;
                        break;
                    }

                case (Countries.Macau): //958
                    {
                        label3.Text = "Макао";
                        textBox1.Text = "Макао";
                        picture.Image = Properties.Resources.mo;
                        break;
                    }

                case (Countries.NoCountry):
                    {
                        textBox1.Text = "";
                        label3.Text = "";
                        picture.Visible = false;
                        codeFound = false;
                        NewScan.Hide();
                        Scan.Show();
                        break;
                    }

                default:
                    {
                        codeFound = false;
                        break;
                    }

            }

            if (codeFound)
            {
                textBox1.Enabled = false;
                picture.Visible = true;
                CurrentCountry = 0;
                label2.Show();
            }
            else
            {
                textBox1.Enabled = Enabled;
                picture.Visible = false;
            }
        }

        ////////////////////////////////////////////////////////////////////
        ////                    END of switch                           ////
        ////                Beginning button click                      ////
        ////////////////////////////////////////////////////////////////////

        private void button1_Click(object sender, EventArgs e) ///New Scan
        {
            picture.Visible = false;
            label3.Text = "";
            textBox1.Enabled = true;
            textBox1.Text = "";
            textBox1.Focus();
            Scan.Show();
            label2.Hide();
        }

        private void button2_Click(object sender, EventArgs e)  //Scan
        {
            if (textBox1.Text == "")
            {

            }
            else
            {
                Scan.Hide();
                textBox1.Enabled = false;
                ChangeMade();
                NewScan.Show();
                NewScan.Focus();
            }

        }

        private void picture_Click(object sender, EventArgs e)
        {

        }

        private void BarcodeCountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            textBox1.Focus();
        }

        private void BarcodeCountry_Load(object sender, EventArgs e)
        {
            label2.Hide();
        }
    }
}
