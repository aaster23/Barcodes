﻿namespace WindowsFormsApplication3
{
    partial class BarcodeCountry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NewScan = new System.Windows.Forms.Button();
            this.Scan = new System.Windows.Forms.Button();
            this.picture = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(12, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(396, 31);
            this.textBox1.TabIndex = 0;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(406, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Въведи първите 3 числа от баркода";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(70, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(286, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Държава на произход на продукта:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(71, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 25);
            this.label3.TabIndex = 4;
            // 
            // NewScan
            // 
            this.NewScan.Location = new System.Drawing.Point(66, 453);
            this.NewScan.Name = "NewScan";
            this.NewScan.Size = new System.Drawing.Size(290, 38);
            this.NewScan.TabIndex = 5;
            this.NewScan.Text = "Ново сканиране";
            this.NewScan.UseVisualStyleBackColor = true;
            this.NewScan.Click += new System.EventHandler(this.button1_Click);
            // 
            // Scan
            // 
            this.Scan.Location = new System.Drawing.Point(66, 453);
            this.Scan.Name = "Scan";
            this.Scan.Size = new System.Drawing.Size(290, 38);
            this.Scan.TabIndex = 1;
            this.Scan.Text = "Сканирай";
            this.Scan.UseVisualStyleBackColor = true;
            this.Scan.Click += new System.EventHandler(this.button2_Click);
            // 
            // picture
            // 
            this.picture.Image = global::WindowsFormsApplication3.Properties.Resources.mo;
            this.picture.Location = new System.Drawing.Point(58, 174);
            this.picture.Name = "picture";
            this.picture.Size = new System.Drawing.Size(303, 211);
            this.picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picture.TabIndex = 2;
            this.picture.TabStop = false;
            this.picture.Visible = false;
            this.picture.Click += new System.EventHandler(this.picture_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(66, 453);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(290, 38);
            this.button1.TabIndex = 5;
            this.button1.Text = "Ново сканиране";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BarcodeCountry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(420, 574);
            this.Controls.Add(this.Scan);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.NewScan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.picture);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.MaximizeBox = false;
            this.Name = "BarcodeCountry";
            this.ShowIcon = false;
            this.Text = "BarcodeCountry";
            this.Load += new System.EventHandler(this.BarcodeCountry_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BarcodeCountry_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.picture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picture;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button NewScan;
        private System.Windows.Forms.Button Scan;
        private System.Windows.Forms.Button button1;
    }
}

